/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/SQLTemplate.sql to edit this template
 */
/**
 * Author:  Dell
 * Created: 14-Jun-2024
 */
-- Creating Job table
CREATE TABLE job (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    status VARCHAR(255) NOT NULL,
    service VARCHAR(255) NOT NULL
);

-- Creating EC2Instance table
CREATE TABLE ec2_instance (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    instance_id VARCHAR(255) NOT NULL
);

-- Creating S3Bucket table
CREATE TABLE s3_bucket (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL
);
-- Creating S3BucketObject table
CREATE TABLE s3_bucket_objects (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    bucket_name VARCHAR(255) NOT NULL,
    object_key VARCHAR(255) NOT NULL
);
