/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.awsdiscovery.aws_discovery_app.service;

import com.example.awsdiscovery.aws_discovery_app.entity.EC2Instance;
import com.example.awsdiscovery.aws_discovery_app.entity.Job;
import com.example.awsdiscovery.aws_discovery_app.entity.S3Bucket;
import com.example.awsdiscovery.aws_discovery_app.entity.S3BucketObject;
import com.example.awsdiscovery.aws_discovery_app.exception.ResourceNotFoundException;
import com.example.awsdiscovery.aws_discovery_app.repository.EC2InstanceRepository;
import com.example.awsdiscovery.aws_discovery_app.repository.JobRepository;
import com.example.awsdiscovery.aws_discovery_app.repository.S3BucketObjectRepository;
import java.util.concurrent.CompletableFuture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import com.example.awsdiscovery.aws_discovery_app.repository.S3BucketRepository;
import java.util.List;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.services.ec2.Ec2Client;
import software.amazon.awssdk.services.ec2.model.DescribeInstancesResponse;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.ListBucketsResponse;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Request;
import software.amazon.awssdk.services.s3.model.ListObjectsV2Response;

/**
 *
 * @author Dell
 */
@Service
public class DiscoveryService {

    @Autowired
    private S3Client s3Client;

    @Autowired
    private Ec2Client ec2Client;

    @Autowired
    private JobRepository jobRepository;

    @Autowired
    private S3BucketRepository s3BucketRepository;

    @Autowired
    private EC2InstanceRepository ec2InstanceRepository;
    @Autowired
    private S3BucketObjectRepository s3BucketObjectRepository;

    @Async
    public CompletableFuture<Void> discoverEC2Instances(Job job) {
        return CompletableFuture.runAsync(() -> {
            try {
                DescribeInstancesResponse response = ec2Client.describeInstances();
                response.reservations().forEach(reservation -> reservation.instances().forEach(instance -> {
                    EC2Instance ec2Instance = new EC2Instance();
                    ec2Instance.setInstanceId(instance.instanceId());
                    ec2InstanceRepository.save(ec2Instance);
                }));
                job.setStatus("Completed");
            } catch (Exception e) {
                job.setStatus("Failed");
            } finally {
                jobRepository.save(job);
            }
        });
    }

    @Async
    public CompletableFuture<Void> discoverS3Buckets(Job job) {
        return CompletableFuture.runAsync(() -> {
            try {
                ListBucketsResponse response = s3Client.listBuckets();
                response.buckets().forEach(bucket -> {
                    S3Bucket s3Bucket = new S3Bucket();
                    s3Bucket.setName(bucket.name());
                    s3BucketRepository.save(s3Bucket);
                });
                job.setStatus("Completed");
            } catch (Exception e) {
                job.setStatus("Failed");
            } finally {
                jobRepository.save(job);
            }
        });
    }

    @Async
    public CompletableFuture<Void> discoverS3BucketObjects(String bucketName, Job job) {
        return CompletableFuture.runAsync(() -> {
            try {
                ListObjectsV2Request request = ListObjectsV2Request.builder().bucket(bucketName).build();
                ListObjectsV2Response response;
                do {
                    response = s3Client.listObjectsV2(request);
                    response.contents().forEach(s3Object -> {
                        S3BucketObject bucketObject = new S3BucketObject();
                        bucketObject.setBucketName(bucketName);
                        bucketObject.setObjectKey(s3Object.key());
                        s3BucketObjectRepository.save(bucketObject);
                    });
                    request = request.toBuilder().continuationToken(response.nextContinuationToken()).build();
                } while (response.isTruncated());
                job.setStatus("Completed");
            } catch (Exception e) {
                job.setStatus("Failed");
            } finally {
                jobRepository.save(job);
            }
        });
    }

    public long countS3BucketObjects(String bucketName) {
        return s3BucketObjectRepository.countByBucketName(bucketName);
    }

    public Job createJob(String service) {
        Job job = new Job();
        job.setStatus("In Progress");
        job.setService(service);
        return jobRepository.save(job);
    }

    public Job getJob(Long jobId) {
        return jobRepository.findById(jobId)
                .orElseThrow(() -> new ResourceNotFoundException("Job not found"));
    }

    public List<S3Bucket> getAllS3Buckets() {
        return s3BucketRepository.findAll();
    }

    public List<EC2Instance> getAllEC2Instances() {
        return ec2InstanceRepository.findAll();
    }

    public List<String> getS3BucketObjectsLike(String bucketName, String pattern) {
        return s3BucketObjectRepository.findByBucketNameAndPattern(bucketName, pattern);
    }

}
