/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.awsdiscovery.aws_discovery_app.controller;

import com.example.awsdiscovery.aws_discovery_app.entity.Job;
import com.example.awsdiscovery.aws_discovery_app.service.DiscoveryService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Dell
 */
@RestController
@RequestMapping("/api")
public class DiscoveryController {
    @Autowired
    private DiscoveryService discoveryService;

    @PostMapping("/discoverServices")
    public ResponseEntity<Long> discoverServices(@RequestBody List<String> services) {
         Long lastJobId = null;
        for (String service : services) {
            Job job = discoveryService.createJob(service);
            lastJobId = job.getId();
            if ("EC2".equalsIgnoreCase(service)) {
                discoveryService.discoverEC2Instances(job);
            }
            if ("S3".equalsIgnoreCase(service)) {
                discoveryService.discoverS3Buckets(job);
            }
        }
        return ResponseEntity.ok(lastJobId);
    }

    @GetMapping("/getJobResult/{jobId}")
    public ResponseEntity<String> getJobResult(@PathVariable Long jobId) {
        Job job = discoveryService.getJob(jobId);
        return ResponseEntity.ok(job.getStatus());
    }

    @GetMapping("/getDiscoveryResult")
    public ResponseEntity<?> getDiscoveryResult(@RequestParam String service) {
        if ("S3".equalsIgnoreCase(service)) {
            return ResponseEntity.ok(discoveryService.getAllS3Buckets());
        } else if ("EC2".equalsIgnoreCase(service)) {
            return ResponseEntity.ok(discoveryService.getAllEC2Instances());
        }
        return ResponseEntity.badRequest().body("Invalid service name");
    }

   @GetMapping("/getS3BucketObjects")
    public ResponseEntity<Long> getS3BucketObjects(@RequestParam String bucketName) {
        Job job = discoveryService.createJob(bucketName);
        discoveryService.discoverS3BucketObjects(bucketName, job);
        return ResponseEntity.ok(job.getId());
    }

    @GetMapping("/getS3BucketObjectCount")
    public ResponseEntity<Long> getS3BucketObjectCount(@RequestParam String bucketName) {
        long count = discoveryService.countS3BucketObjects(bucketName);
        return ResponseEntity.ok(count);
    }

     @GetMapping("/getS3BucketObjectLike")
    public ResponseEntity<List<String>> getS3BucketObjectLike(@RequestParam String bucketName, @RequestParam String pattern) {
        List<String> matchingObjects = discoveryService.getS3BucketObjectsLike(bucketName, pattern);
        return ResponseEntity.ok(matchingObjects);
    }
}
