/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.awsdiscovery.aws_discovery_app.repository;

import com.example.awsdiscovery.aws_discovery_app.entity.S3BucketObject;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Dell
 */
public interface S3BucketObjectRepository extends JpaRepository<S3BucketObject, Long> {

    long countByBucketName(String bucketName);

    @Query("SELECT s.objectKey FROM S3BucketObject s WHERE s.bucketName = :bucketName AND s.objectKey LIKE %:pattern%")
    List<String> findByBucketNameAndPattern(@Param("bucketName") String bucketName, @Param("pattern") String pattern);

}
