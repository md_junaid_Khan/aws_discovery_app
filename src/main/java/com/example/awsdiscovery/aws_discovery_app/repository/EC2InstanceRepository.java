/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.example.awsdiscovery.aws_discovery_app.repository;

import com.example.awsdiscovery.aws_discovery_app.entity.EC2Instance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Dell
 */
@Repository
public interface EC2InstanceRepository extends JpaRepository<EC2Instance, Long> {

}
