# AWS Discovery Service

## Overview
This application discovers AWS EC2 instances and S3 buckets in the Mumbai region and persists the results in a database. It supports asynchronous discovery and provides endpoints to check the status and results of the discovery jobs.

## Endpoints
### Discover Services
- *POST* /api/jobs/discoverServices
- *Input*: List of services (e.g., ["EC2", "S3"])
- *Output*: Job ID

### Get Job Status
- *GET* /api/getJobResult/{jobId}
- *Input*: Job ID
- *Output*: Status (Success, In Progress, Failed)

### Get Discovery Result
- *GET* /api/getDiscoveryResult
- *Input*: Service name (EC2 or S3) (e.g.,@RequestParam String service)
- *Output*: List of EC2 instances or S3 buckets

### Get S3 Bucket Objects
- *POST* /api/getS3BucketObjects
- *Input*: S3 bucket name (e.g.,@RequestParam String bucketName)
- *Output*: Job ID

## Database
### Schema
- jobs: Stores job details and status i.e
--- Creating Job table
CREATE TABLE job (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    status VARCHAR(255) NOT NULL,
    service VARCHAR(255) NOT NULL
);
- ec2_instances: Stores discovered EC2 instance IDs i.e
--- Creating EC2Instance table
CREATE TABLE ec2_instance (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    instance_id VARCHAR(255) NOT NULL
);
- s3_buckets: Stores discovered S3 bucket names i.e
-- Creating S3Bucket table
CREATE TABLE s3_bucket (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL
);

- s3_bucket_objects: Stores objects within a specific S3 bucket i.e
-- Creating S3BucketObject table
CREATE TABLE s3_bucket_objects (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    bucket_name VARCHAR(255) NOT NULL,
    object_key VARCHAR(255) NOT NULL
);

### SQL Scripts
Refer to the sql folder for table creation scripts.

## Configuration
- application.properties contains database configuration details.

## Running the Application
1. Clone the repository.
2. Configure the database in application.properties.
3. Run the application using mvn spring-boot:run.

## Dependencies
- Spring Boot
- AWS SDK for Java
- H2 DataBase